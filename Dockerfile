FROM python:latest
RUN apt install python
COPY . /src/
CMD ['python','/src/main.py']
